const SQLiteModel = function(sqlite){

    //method to init table with any attributes 

    this.init = function(table,params){
        return new Promise((resolve,reject)=>{
            sqlite.serialize(function(){
                let query = 'CREATE TABLE IF NOT EXISTS ' +table+ ' (id INTEGER PRIMARY KEY, '
                let element ='';           
                for(let [key,value] of Object.entries(params)){
                    element+=key+' '+value+', ';
                }
                query+=element.substring(0, element.length-2);
                query+=');';
                //console.log(query);
                sqlite.run(query, function(response, error){
                    if(error){
                        let info = {
                            message: error.message,
                            table: table,
                            params: params
                        }
                        reject(info);
                    }
                    resolve(params);
                });    
            });
        });      
    };

    //method to drop table

    this.clear = function(table){
        return new Promise((resolve,reject)=>{
            sqlite.serialize(function(){
                let query = 'DROP TABLE IF EXISTS ' +table;             
                //console.log(query);
                sqlite.run(query, function(response, error){
                    if(error){
                        let info = {
                            message: error.message,
                            table: table                            
                        }
                        reject(info);
                    }
                    resolve('Drop table: '+table);
                });   

            });
        });
    };

    //method to insert to table row id is automatic

    this.insert = function(table,params){
        return new Promise((resolve,reject)=>{
            sqlite.serialize(function(){
                let query = 'INSERT INTO ' +table+ ' ('
                let columnNames = '';
                let columnValues = '';

                for(let [key,value] of Object.entries(params)){
                    columnNames+="'"+key+"', ";
                    if(isNaN(value)){
                        columnValues+="'"+value+"', ";
                    }else{
                        columnValues+=value+",";
                    }
                }

                query+=columnNames.substring(0,columnNames.length-2);
                query+=') VALUES (';
                query+=columnValues.substring(0,columnValues.length-2);
                query+=');';

                //console.log(query);
                sqlite.run(query, function(response, error){
                    if(error){
                        let info = {
                            message: error.message,
                            table: table,
                            params: params
                        }
                        reject(info);
                    }else{
                        resolve(params);
                    }
                    
                }); 

            });
        });    
    };

    //method to show all rows of table 

    this.getAll = function(table){
        return new Promise((resolve,reject)=>{
            sqlite.serialize(function(){
                sqlite.all("SELECT * FROM "+table,function(error,rows){
                    if (error) {
                        let info = {
                            message: error.message,
                            table: table,
                            id: id
                        }
                        reject(info);
                    } else {
                        resolve(rows);
                    }
                });
            });
        });        
    };

    //method to obtain data row since id

    this.getById = function(table,id){
        return new Promise((resolve,reject)=>{
            sqlite.serialize(function(){
                sqlite.all('SELECT * FROM '+table+' WHERE id= '+id,function(error,rows){
                    if (error) {
                        let info = {
                            message: error.message,
                            table: table,
                            id: id
                        }
                        reject(info);
                    } else {
                        resolve(rows[0]);
                    }
                });
            });
        });    
    };

    //method to update rows with attributes of init
    
    this.update = function(table,params,id){
        return new Promise((resolve,reject)=>{
            sqlite.serialize(function(){
                let query = 'UPDATE ' +table+ ' SET '
                let element ='';
                
                for(let [key,value] of Object.entries(params)){
                    element+=key+'=';
                    if(isNaN(value)){
                        element+='"'+value+'", ';
                    }else{
                        element+=value+', ';
                    }
                }

                query+=element.substring(0, element.length-2);
                query+=' WHERE id='+id;
                
                //console.log(query);
                sqlite.run(query, function(response, error){
                    if(error){
                        let info = {
                            message: error.message,
                            table: table,
                            params: params,
                            id: id
                        }
                        reject(info);
                    }
                    resolve(params);
                });   
            });
        });      
    };

    //method to eliminate one row since id

    this.delete = function(table,id){
        return new Promise((resolve,reject)=>{
            sqlite.serialize(function(){
                let query = 'DELETE FROM ' +table+ ' WHERE id='+id;             
                //console.log(query);
                sqlite.run(query, function(response, error){
                    if(error){
                        let info = {
                            message: error.message,
                            table: table,
                            id: id
                        }
                        reject(info);
                    }
                    resolve('Se ha eliminado '+id);
                });    

            });
        });           
    };
 
    return this;
};

module.exports = SQLiteModel;