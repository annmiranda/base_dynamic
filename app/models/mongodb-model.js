const config = require ('../../config.json');
let ObjectId = require('mongodb').ObjectId;

const MongoDBModel = function(MongoClient,url){
    
    this.init = function(table,params){
        return new Promise((resolve,reject)=>{
            resolve('Not aplicate to MongoDB'); //because you can insert objects and atributtes additionals in insert and update
        });      
    };

    //method to drop collection

    this.clear = function(table){
        return new Promise((resolve,reject)=>{
            MongoClient.connect(url,function(error,client){
                if(error){
                    console.error(error);
                    reject(error);
                }else{
                    let database = client.db(config.database.mongodb.databaseName);
                    const collection = database.collection(table);                           
                    collection.drop({}, function(errorClear,result){
                        if (errorClear) {
                            reject(errorClear);
                        } else {
                            resolve(result);
                            client.close();
                        }  
                    });
                }
            });
        });
    };

    //method to insert in collection one object id is automatic

    this.insert = function(table,params){
        return new Promise((resolve,reject)=>{
            MongoClient.connect(url,function(error,client){
                if(error){
                    console.error(error);
                    reject(error);
                }else{
                    let database = client.db(config.database.mongodb.databaseName);
                    const collection = database.collection(table);                  
                    
                    collection.insertOne(params,function(errorInsert,result){
                        if (errorInsert) {
                            reject(errorInsert);
                        } else {
                            resolve(result);
                            client.close();
                        }  
                    });
                }
            });
        });    
    };

    //method to show all objetcs of a collection  

    this.getAll = function(table){
        return new Promise((resolve,reject)=>{
            MongoClient.connect(url,function(error,client){
                if(error){
                    console.error(error);
                    reject(error);
                }else{
                    let database = client.db(config.database.mongodb.databaseName);
                    const collection = database.collection(table);                  
                    collection.find({}).toArray(function(errorGetAll,result){
                        resolve(result);
                        client.close();
                    })
                }
            });
        });        
    };

    //method to obtain data object since id

    this.getById = function(table,id){
        return new Promise((resolve,reject)=>{
            MongoClient.connect(url,function(error,client){
                if(error){
                    console.error(error);
                    reject(error);
                }else{
                    let database = client.db(config.database.mongodb.databaseName);
                    const collection = database.collection(table);                  
                    collection.findOne({_id: new ObjectId(id)},function(errorById,result){
                        if (errorById) {
                            reject(errorById);
                        } else {
                            resolve(result);
                            client.close();
                        }   
                    });
                }
            });
        });    
    };

    //method to update objects with attributes

    this.update = function(table,params,id){
        return new Promise((resolve,reject)=>{
            MongoClient.connect(url,function(error,client){
                if(error){
                    console.error(error);
                    reject(error);
                }else{
                    let database = client.db(config.database.mongodb.databaseName);
                    const collection = database.collection(table);                  
                    
                    collection.updateOne({ _id: new ObjectId(id) }, { $set: params}, function(errorUpdate,result){
                        if (errorUpdate) {
                            reject(errorUpdate);
                        } else {
                            resolve(result);
                            client.close();
                        }  
                    });
                }
            });
        });       
    };

    //method to eliminate one object since id

    this.delete = function(table,id){
        return new Promise((resolve,reject)=>{
            MongoClient.connect(url,function(error,client){
                if(error){
                    console.error(error);
                    reject(error);
                }else{
                    let database = client.db(config.database.mongodb.databaseName);
                    const collection = database.collection(table);                  
                    
                    collection.deleteOne({ _id: new ObjectId(id) }, function(errorDelete,result){
                        if (errorDelete) {
                            reject(errorDelete);
                        } else {
                            resolve(result);
                            client.close();
                        }  
                    });
                }
            });
        });           
    };
    
    return this;
}

module.exports = MongoDBModel;